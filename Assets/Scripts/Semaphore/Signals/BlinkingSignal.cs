using System.Collections.Generic;
using UnityEngine;

namespace Semaphore.Signals
{
	/// <summary>
	/// While activated changes on\off state with defined period
	/// </summary>
	public class BlinkingSignal : Signal
	{
		[SerializeField] private List<GameObject> targetObjects;
		[SerializeField] private float onTime;
		[SerializeField] private float offTime;

		private float _switchTime;
		private bool _isOn;

		public override void Activate()
		{
			enabled = true;
			_switchTime = Time.time + onTime;
			_isOn = true;
			SetObjectsActive(true);
		}

		public override void Deactivate()
		{
			enabled = false;
		}

		private void Update()
		{
			var curTime = Time.time;
			if (curTime < _switchTime) return;

			_isOn = !_isOn;
			_switchTime = curTime + (_isOn ? onTime : offTime);
			SetObjectsActive(_isOn);
		}

		private void OnDisable()
		{
			SetObjectsActive(false);
		}

		private void SetObjectsActive(bool isActive)
		{
			foreach (var targetObject in targetObjects)
			{
				targetObject.SetActive(isActive);
			}
		}
	}
}