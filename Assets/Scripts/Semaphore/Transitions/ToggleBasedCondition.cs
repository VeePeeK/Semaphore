namespace Semaphore.Transitions
{
	/// <summary>
	/// Condition controlled by a simple UI Toggle as for example
	/// </summary>
	public class ToggleBasedCondition : TransitionCondition
	{
		private bool _toggle;
		public override bool IsSatisfied()
		{
			return _toggle;
		}

		public void OnToggle(bool isOn)
		{
			_toggle = isOn;
		}
	}
}